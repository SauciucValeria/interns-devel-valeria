/**
 * Created by Sauciuc on 5/3/2017.
 */

function initDialog(element) {
    $.confirm({
        title: 'Confirm!',
        style: 'position: absolute;',
        type: 'blue',
        typeAnimated: true,
        content: 'Are you sure you want to delete element?',
        buttons: {
            confirm: function () {
                element.firstChild.click();
            },
            cancel: function () {
                $.alert('Canceled!');
            }

        }
    });
}

function showEdit(element) {
    element.firstChild.style.visibility = "visible";
}

function openDialogFunc(){

    $.confirm({
        title: 'Confirm!',
        style: 'position: absolute;',
        type: 'blue',
        typeAnimated: true,
        content: 'Are you sure you want to delete element?',
        buttons: {
            confirm: function () {
               // $("#myHiddenButtonID").trigger("click");
                $('#myHiddenButtonID').click();
                //$.alert('Confirmed!');
            },
            cancel: function () {
                $.alert('Canceled!');
            }

        }
    });
}

