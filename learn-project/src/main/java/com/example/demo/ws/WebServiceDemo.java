package com.example.demo.ws;

import com.example.demo.spring.HelloWorldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * @author Tiberiu Pasat
 */
@Component
@Path("/hello/{name}")
public class WebServiceDemo {
    @Autowired private HelloWorldService helloWorldService;

    @Path("/")
    @GET
    public String hello(@QueryParam("name") String name) {

        return "<html> " + "<title>" + "Hello Jersey" + "</title>"
                + "<body><h1>" + "Hello " + name + "</body></h1>" + "</html> ";
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    public String sayHtmlHello(@PathParam("name") String name) {
        return "<html> " + "<title>" + "Hello Jersey" + "</title>"
                + "<body><h1>" + "Hello " +name+ "</body></h1>" + "</html> ";
    }

}
