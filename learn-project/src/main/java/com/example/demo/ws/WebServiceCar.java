package com.example.demo.ws;

import com.example.demo.jpa.model.CarEntity;
import com.example.demo.spring.CarServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by Sauciuc on 5/16/2017.
 */
@Component
@Path("/cars")
public class WebServiceCar {
    @Autowired
    private CarServiceImpl carService;


    @GET
    @Produces({ MediaType.APPLICATION_JSON})
    public List<CarEntity> findAll() {
        List<CarEntity> carList =  (List<CarEntity>) carService.findAll();
        return carList;
    }

    @GET @Path("search/{query}")
    @Produces({ MediaType.APPLICATION_JSON})
    public List<CarEntity> findByName(@PathParam("query") String query) {
        return carService.findByName(query);
    }


}
