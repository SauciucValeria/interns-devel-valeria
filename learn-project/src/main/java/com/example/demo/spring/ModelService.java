package com.example.demo.spring;

import com.example.demo.jpa.model.CarModelEntity;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by Sauciuc on 4/25/2017.
 */
public interface ModelService extends CrudRepository<CarModelEntity, Long> {

}
