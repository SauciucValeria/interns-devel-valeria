package com.example.demo.spring;

/**
 * @author Tiberiu Pasat
 */
public interface HelloWorldService {
    String hello(String name);
}
