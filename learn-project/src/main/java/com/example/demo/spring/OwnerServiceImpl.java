package com.example.demo.spring;

import com.example.demo.jpa.model.CarModelEntity;
import com.example.demo.jpa.model.OwnerEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

/**
 * Created by Sauciuc on 5/11/2017.
 */
@Service
public class OwnerServiceImpl implements  OwnerService{

    @PersistenceContext
    private EntityManager entitymanager;
    @Override
    public <S extends OwnerEntity> S save(S entity) {
        return null;
    }

    @Override
    public <S extends OwnerEntity> Iterable<S> save(Iterable<S> entities) {
        return null;
    }

    @Override
    public OwnerEntity findOne(Long aLong) {
        return  entitymanager.find(OwnerEntity.class, aLong);
    }

    @Override
    public boolean exists(Long aLong) {
        return false;
    }

    @Override
    public Iterable<OwnerEntity> findAll() {
        return null;
    }

    @Override
    public Iterable<OwnerEntity> findAll(Iterable<Long> longs) {
        return entitymanager.createQuery(" from OwnerEntity ", OwnerEntity.class)
                .getResultList();
    }

    @Override
    public long count() {
        return 0;
    }


    @Override
    public void delete(Long aLong) {

    }

    @Transactional
    @Override
    public void delete(OwnerEntity entity) {
        entitymanager.remove(entitymanager.contains(entity) ? entity : entitymanager.merge(entity));

    }

    @Override
    public void delete(Iterable<? extends OwnerEntity> entities) {

    }

    @Override
    public void deleteAll() {

    }

    @Transactional
    public void saveOwner(OwnerEntity owner){
        entitymanager.persist(owner);

    }

    @Transactional
    public void updateOwner(OwnerEntity owner){
        entitymanager.merge(owner);

    }
}
