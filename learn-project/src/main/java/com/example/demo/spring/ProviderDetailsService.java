package com.example.demo.spring;

import com.example.demo.jpa.model.ProviderDetailsEntity;
import com.example.demo.jpa.model.ProviderEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Sauciuc on 5/2/2017.
 */
public interface ProviderDetailsService extends CrudRepository<ProviderDetailsEntity, Long> {
}
