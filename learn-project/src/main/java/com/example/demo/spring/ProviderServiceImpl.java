package com.example.demo.spring;

import com.example.demo.jpa.model.CarModelEntity;
import com.example.demo.jpa.model.ProviderEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

/**
 * Created by Sauciuc on 4/28/2017.
 */
@Service
public class ProviderServiceImpl implements ProviderService{

    @PersistenceContext
    private EntityManager entitymanager;
    @Override
    public <S extends ProviderEntity> S save(S entity) {
        return null;
    }

    @Override
    public <S extends ProviderEntity> Iterable<S> save(Iterable<S> entities) {
        return null;
    }

    @Override
    public ProviderEntity findOne(Long aLong) {
        return  entitymanager.find(ProviderEntity.class, aLong);
    }

    @Override
    public boolean exists(Long aLong) {
        return false;
    }

    @Override
    public Iterable<ProviderEntity> findAll() {
        return entitymanager.createQuery(" from ProviderEntity ", ProviderEntity.class)
                .getResultList();
    }

    @Override
    public Iterable<ProviderEntity> findAll(Iterable<Long> longs) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void delete(Long aLong) {

    }

    @Transactional
    @Override
    public void delete(ProviderEntity entity) {
        entitymanager.remove(entitymanager.contains(entity) ? entity : entitymanager.merge(entity));

    }

    @Override
    public void delete(Iterable<? extends ProviderEntity> entities) {

    }

    @Override
    public void deleteAll() {

    }

    @Transactional
    public void saveProvider(ProviderEntity provider){
        entitymanager.persist(provider);

    }

    @Transactional
    public void updateProvider(ProviderEntity provider){
        entitymanager.merge(provider);

    }
}
