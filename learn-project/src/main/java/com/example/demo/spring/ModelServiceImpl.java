package com.example.demo.spring;

import com.example.demo.jpa.model.CarModelEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

/**
 * Created by Sauciuc on 4/25/2017.
 */
@Service
public class ModelServiceImpl implements ModelService {

    @PersistenceContext
    private EntityManager entitymanager;
    @Override
    public <S extends CarModelEntity> S save(S entity) {
        return null;
    }

    @Override
    public <S extends CarModelEntity> Iterable<S> save(Iterable<S> entities) {
        return null;
    }

    @Override
    public CarModelEntity findOne(Long aLong) {
        return  entitymanager.find(CarModelEntity.class, aLong);

    }

    @Override
    public boolean exists(Long aLong) {
        return false;
    }

    @Override
    public Iterable<CarModelEntity> findAll() {
        return entitymanager.createQuery(" from CarModelEntity ", CarModelEntity.class)
                .getResultList();

    }


    public Iterable<CarModelEntity> findAllByCategId(Long categId) {
        return entitymanager.createNativeQuery(" select * from CAR_MODEL c where c.CATEGORY_ENTITY_ID = ? ", CarModelEntity.class).setParameter(1, categId)
                .getResultList();

    }

    @Override
    public Iterable<CarModelEntity> findAll(Iterable<Long> longs) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void delete(Long aLong) {

    }

    @Transactional
    @Override
    public void delete(CarModelEntity entity) {
        entitymanager.remove(entitymanager.contains(entity) ? entity : entitymanager.merge(entity));
    }

    @Override
    public void delete(Iterable<? extends CarModelEntity> entities) {

    }

    @Override
    public void deleteAll() {

    }

    @Transactional
    public void saveModel(CarModelEntity model){
        entitymanager.persist(model);

    }

    @Transactional
    public void updateModel(CarModelEntity model){
        entitymanager.merge(model);

    }
}
