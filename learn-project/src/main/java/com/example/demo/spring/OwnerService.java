package com.example.demo.spring;

import com.example.demo.jpa.model.CarModelEntity;
import com.example.demo.jpa.model.OwnerEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Sauciuc on 5/11/2017.
 */
public interface OwnerService extends CrudRepository<OwnerEntity, Long> {
}
