package com.example.demo.spring;

import com.example.demo.jpa.model.ProviderEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Sauciuc on 4/28/2017.
 */
public interface ProviderService extends CrudRepository<ProviderEntity, Long> {
}
