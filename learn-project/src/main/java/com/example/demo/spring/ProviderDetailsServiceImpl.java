package com.example.demo.spring;

import com.example.demo.jpa.model.ProviderDetailsEntity;
import org.springframework.stereotype.Service;

/**
 * Created by Sauciuc on 5/2/2017.
 */
@Service
public class ProviderDetailsServiceImpl implements ProviderDetailsService{

    @Override
    public <S extends ProviderDetailsEntity> S save(S entity) {
        return null;
    }

    @Override
    public <S extends ProviderDetailsEntity> Iterable<S> save(Iterable<S> entities) {
        return null;
    }

    @Override
    public ProviderDetailsEntity findOne(Long aLong) {
        return null;
    }

    @Override
    public boolean exists(Long aLong) {
        return false;
    }

    @Override
    public Iterable<ProviderDetailsEntity> findAll() {
        return null;
    }

    @Override
    public Iterable<ProviderDetailsEntity> findAll(Iterable<Long> longs) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void delete(Long aLong) {

    }

    @Override
    public void delete(ProviderDetailsEntity entity) {

    }

    @Override
    public void delete(Iterable<? extends ProviderDetailsEntity> entities) {

    }

    @Override
    public void deleteAll() {

    }
}
