package com.example.demo.spring;

import com.example.demo.jpa.model.CategoryEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

/**
 * Created by Sauciuc on 4/20/2017.
 */

@Service
public class CategoryServiceImpl implements CategoryService {

    @PersistenceContext
    private EntityManager entitymanager;


    @Override
    public <S extends CategoryEntity> S save(S entity) {
        return null;
    }

    @Override
    public <S extends CategoryEntity> Iterable<S> save(Iterable<S> entities) {
        return null;
    }

    @Override
    public CategoryEntity findOne(Long aLong) {

        return entitymanager.find(CategoryEntity.class, aLong);
    }

    @Override
    public boolean exists(Long aLong) {
        return false;
    }

    @Override
    public Iterable<CategoryEntity> findAll() {

        return entitymanager.createQuery(" from CategoryEntity order by name", CategoryEntity.class)
                .getResultList();
    }


    @Override
    public Iterable<CategoryEntity> findAll(Iterable<Long> longs) {
        return  null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void delete(Long aLong) {

    }


    @Transactional
    @Override
    public void delete(CategoryEntity entity) {
        entitymanager.remove(entitymanager.contains(entity) ? entity : entitymanager.merge(entity));

    }

    @Override
    public void delete(Iterable<? extends CategoryEntity> entities) {

    }

    @Override
    public void deleteAll() {

    }


    @Transactional
    public void saveCategory(CategoryEntity categ){
        entitymanager.persist(categ);


    }

    @Transactional
    public void updateCategory(CategoryEntity categ){
        entitymanager.merge(categ);

    }

    public CategoryEntity findByName(String name) {

        return entitymanager.createQuery(" from CategoryEntity c where name=c.name", CategoryEntity.class).getSingleResult();
    }



}
