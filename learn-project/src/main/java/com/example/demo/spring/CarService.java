package com.example.demo.spring;

import com.example.demo.jpa.model.CarEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Sauciuc on 4/20/2017.
 */
public interface CarService extends CrudRepository<CarEntity, Long> {

}
