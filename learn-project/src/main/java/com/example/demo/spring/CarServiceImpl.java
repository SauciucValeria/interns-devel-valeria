package com.example.demo.spring;

import com.example.demo.jpa.model.CarEntity;
import com.example.demo.jpa.model.CategoryEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Sauciuc on 4/20/2017.
 */
@Service
public class CarServiceImpl implements CarService {

    @PersistenceContext
    private EntityManager entitymanager;


    @Override
    public <S extends CarEntity> S save(S entity) {
        return null;
    }

    @Override
    public <S extends CarEntity> Iterable<S> save(Iterable<S> entities) {
        return null;
    }

    @Override
    public CarEntity findOne(Long aLong) {
        return entitymanager.find(CarEntity.class, aLong);
    }

    @Override
    public boolean exists(Long aLong) {
        return false;
    }

    @Override
    public Iterable<CarEntity> findAll() {

        List<CarEntity> carList = entitymanager.createQuery(" from CarEntity ", CarEntity.class)
                .getResultList();

        return carList;

    }

    @Override
    public Iterable<CarEntity> findAll(Iterable<Long> longs) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void delete(Long aLong) {

    }

    @Transactional
    @Override
    public void delete(CarEntity entity) {
        entitymanager.remove(entitymanager.contains(entity) ? entity : entitymanager.merge(entity));
    }

    @Override
    public void delete(Iterable<? extends CarEntity> entities) {

    }

    @Override
    public void deleteAll() {

    }

    @Transactional
    public void saveCar(CarEntity carEntity){
        entitymanager.persist(carEntity);

    }

    @Transactional
    public void updateCar(CarEntity carEntity){
        entitymanager.merge(carEntity);

    }



    public List<CarEntity> findByName(String query) {
//        return entitymanager.createQuery(" from CarEntity c where c.name like  ", CarEntity.class)
//                .getResultList();

        List<CarEntity> carList =  entitymanager.createNativeQuery(" select * from CAR cr join CAR_MODEL  c on cr.CAR_MODEL_ENTITY_ID = c.id where upper(c.NAME) like upper(\'%\'||?) or upper(c.name) like upper(?||\'%\') or upper(c.name)=upper(?)",
                CarEntity.class).setParameter(1, query).setParameter(2,query).setParameter(3,query)
                .getResultList();
        for (CarEntity car:carList){
            car.getCarModelEntity().setCarList(null);
            car.getCarModelEntity().getCategoryEntity().setModelList(null);
        }
        return carList;
    }
}
