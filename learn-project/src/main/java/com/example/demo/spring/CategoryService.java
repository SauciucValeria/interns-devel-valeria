package com.example.demo.spring;


import com.example.demo.jpa.model.CategoryEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Sauciuc on 4/20/2017.
 */
public interface CategoryService extends CrudRepository<CategoryEntity, Long> {

}
