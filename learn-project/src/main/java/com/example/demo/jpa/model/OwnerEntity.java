package com.example.demo.jpa.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Sauciuc on 5/11/2017.
 */

@Entity
@Table(name = "CAR_OWNER")
public class OwnerEntity {

    private  Long id;
    private String firstName;
    private String lastName;
    private String nrTel;
    private List<CarEntity> cars;



    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }



    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNrTel() {
        return nrTel;
    }

    public void setNrTel(String nrTel) {
        this.nrTel = nrTel;
    }

    @OneToMany(mappedBy = "owner")
    public List<CarEntity> getCars() {
        return cars;
    }

    public void setCars(List<CarEntity> cars) {
        this.cars = cars;
    }
}
