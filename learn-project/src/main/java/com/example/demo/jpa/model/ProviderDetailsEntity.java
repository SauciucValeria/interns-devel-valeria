package com.example.demo.jpa.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;

/**
 * Created by Sauciuc on 5/2/2017.
 */
@Entity
@Table(name = "PROVIDER_DETAILS")
public class ProviderDetailsEntity {
    private Long id;
    private ProviderEntity provider;
    private String city;
    private String address;
    private String country;

    @Id
    @GeneratedValue(generator = "foreign")
    @GenericGenerator(name="foreign", strategy="foreign", parameters=@Parameter(name="property", value="provider"))
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @JsonIgnore
    @OneToOne
    @PrimaryKeyJoinColumn
    public ProviderEntity getProvider() {
        return provider;
    }

    public void setProvider(ProviderEntity provider) {
        this.provider = provider;
    }


    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
