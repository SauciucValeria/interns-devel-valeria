package com.example.demo.jpa.model;

import com.google.gson.Gson;
import org.springframework.stereotype.Component;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 * Created by Sauciuc on 5/11/2017.
 */
@Component
public class ModelConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value != null) {
            return new Gson().fromJson(value, CarModelEntity.class);
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        CarModelEntity carModelEntity = (CarModelEntity) value;
        carModelEntity.setCarList(null);
        carModelEntity.getCategoryEntity().setModelList(null);
        return new Gson().toJson(carModelEntity);
    }
}
