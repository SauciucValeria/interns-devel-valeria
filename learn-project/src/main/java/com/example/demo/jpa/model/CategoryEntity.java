package com.example.demo.jpa.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.persistence.Id;
import java.util.List;

/**
 * Created by Sauciuc on 4/14/2017.
 */


@Entity
@Table(name = "CATEGORY")
public class CategoryEntity {

    private String name;
    private long id;
    private List<CarModelEntity> modelList;


    @Id
    @GeneratedValue
    public long getId() {
        return id;
    }

    @Column(name = "NAME", nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(long id) {
        this.id = id;
    }

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "categoryEntity", cascade = CascadeType.REMOVE)
    public List<CarModelEntity> getModelList() {
        return modelList;
    }

    public void setModelList(List<CarModelEntity> modelList) {
        this.modelList = modelList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CategoryEntity that = (CategoryEntity) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
