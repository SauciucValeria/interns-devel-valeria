package com.example.demo.jpa;

import com.example.demo.jpa.model.DemoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;

/**
 * @author Tiberiu Pasat
 */
@Service
public class DemoEntityServiceImpl implements DemoEntityService {
    @Autowired private EntityManager entityManager;

    @Override
    public DemoEntity find(long id) {
        return entityManager.find(DemoEntity.class, id);
    }
}
