package com.example.demo.jpa.model;

import com.google.gson.*;

import org.springframework.stereotype.Component;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;


/**
 * Created by Sauciuc on 5/10/2017.
 */
@Component
public class CategoryConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value != null) {
            return new Gson().fromJson(value, CategoryEntity.class);
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        CategoryEntity carModelEntity = (CategoryEntity) value;
        carModelEntity.setModelList(null);
        return new Gson().toJson(carModelEntity);
    }
}
