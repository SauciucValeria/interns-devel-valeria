package com.example.demo.jpa.model;

import javax.persistence.*;
import java.util.List;


/**
 * Created by Sauciuc on 4/28/2017.
 */

@Entity
@Table(name = "PROVIDER")
public class ProviderEntity {
    private Long id;
    private String name;
    private ProviderDetailsEntity providerDetails;



    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    @OneToOne(mappedBy = "provider", cascade = CascadeType.ALL)
    public ProviderDetailsEntity getProviderDetails() {
        return providerDetails;
    }

    public void setProviderDetails(ProviderDetailsEntity providerDetails) {
        this.providerDetails = providerDetails;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProviderEntity that = (ProviderEntity) o;

        return id != null ? id.equals(that.id) : that.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
