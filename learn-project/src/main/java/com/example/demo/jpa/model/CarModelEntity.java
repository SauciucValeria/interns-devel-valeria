package com.example.demo.jpa.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.annotation.PostConstruct;
import javax.persistence.*;
import java.util.List;

/**
 * Created by Sauciuc on 4/20/2017.
 */

@Entity
@Table(name = "CAR_MODEL")
public class CarModelEntity {

    @Transient
    private Long selectedId;

    @Id
    @GeneratedValue
    private long id;
    private String name;

    @ManyToOne
    @JoinColumn(name = "CATEGORY_ENTITY_ID")
    private CategoryEntity categoryEntity;

    //@OneToMany(mappedBy = "topic", cascade = CascadeType.ALL)
    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "carModelEntity", cascade = CascadeType.REMOVE)
    private List<CarEntity> carList;






    public long getId() {
        return id;
    }

    public String getName() {
//        System.out.println();
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(long id) {
        this.id = id;
    }

    public CategoryEntity getCategoryEntity() {
        return categoryEntity;
    }

    public void setCategoryEntity(CategoryEntity categoryEntity) {
        this.categoryEntity = categoryEntity;
    }

    public List<CarEntity> getCarList() {
        return carList;
    }

    public void setCarList(List<CarEntity> carList) {
        this.carList = carList;
    }

    public Long getSelectedId() {
        return selectedId == null ? this.categoryEntity.getId(): this.selectedId;
    }

    public void setSelectedId(Long selectedId) {
        this.selectedId = selectedId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarModelEntity that = (CarModelEntity) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
