package com.example.demo.jpa;

import com.example.demo.jpa.model.DemoEntity;

/**
 * @author Tiberiu Pasat
 */
public interface DemoEntityService {

    DemoEntity find(long id);
}
