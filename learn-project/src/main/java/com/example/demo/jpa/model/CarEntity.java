package com.example.demo.jpa.model;

import com.example.demo.jsf.Fuel;
import com.example.demo.jsf.PolluantEmissions;
import com.example.demo.jsf.Transmission;
import org.hibernate.annotations.Type;

import javax.persistence.*;

import static javax.persistence.FetchType.LAZY;

/**
 * Created by Sauciuc on 4/11/2017.
 */

@Entity
@Table(name = "CAR")
public class CarEntity {

    private long id;
    private CarModelEntity carModelEntity;
    private Fuel fuel;
    private String color;
    private Integer year;
    private Integer engine;
    private Transmission transmission;
    private PolluantEmissions polluant_emissions;
    private Integer doors_nr;
    private boolean abs;
    private boolean esp;
    private boolean ac;
    private boolean parking_sensors;
    private OwnerEntity owner;
    private ProviderEntity provider;
    private Integer price;



    private byte[] picture;

    @Id
    @GeneratedValue
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @ManyToOne
    public CarModelEntity getCarModelEntity() {
        return carModelEntity;
    }

    public void setCarModelEntity(CarModelEntity carModelEntity) {
        this.carModelEntity = carModelEntity;
    }

    @Enumerated(EnumType.ORDINAL)
    public Fuel getFuel() {
        return fuel;
    }

    public void setFuel(Fuel fuel) {
        this.fuel = fuel;
    }

    public Integer getEngine() {
        return engine;
    }

    public void setEngine(Integer engine) {
        this.engine = engine;
    }

    @Enumerated(EnumType.ORDINAL)
    public Transmission getTransmission() {
        return transmission;
    }

    public void setTransmission(Transmission transmission) {
        this.transmission = transmission;
    }

    @Enumerated(EnumType.ORDINAL)
    public PolluantEmissions getPolluant_emissions() {
        return polluant_emissions;
    }

    public void setPolluant_emissions(PolluantEmissions polluant_emissions) {
        this.polluant_emissions = polluant_emissions;
    }

    @ManyToOne
    public OwnerEntity getOwner() {
        return owner;
    }

    public void setOwner(OwnerEntity owner) {
        this.owner = owner;
    }

    public Integer getDoors_nr() {
        return doors_nr;
    }

    public void setDoors_nr(Integer doors_nr) {
        this.doors_nr = doors_nr;
    }



    public void setAbs(boolean abs) {
        this.abs = abs;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    @Type(type="yes_no")
    public boolean isAbs() {
        return abs;
    }

    @Type(type="yes_no")
    public boolean isEsp() {
        return esp;
    }

    public void setEsp(boolean esp) {
        this.esp = esp;
    }

    @Type(type="yes_no")
    public boolean isAc() {
        return ac;
    }

    public void setAc(boolean ac) {
        this.ac = ac;
    }

    @Type(type="yes_no")
    public boolean isParking_sensors() {
        return parking_sensors;
    }

    public void setParking_sensors(boolean parking_sensors) {
        this.parking_sensors = parking_sensors;
    }

    @Basic(fetch=LAZY)
    @Lob @Column(name="IMAGE")
    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    @ManyToOne
    public ProviderEntity getProvider() {
        return provider;
    }

    public void setProvider(ProviderEntity provider) {
        this.provider = provider;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
