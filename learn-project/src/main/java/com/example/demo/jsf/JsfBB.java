package com.example.demo.jsf;

import com.example.demo.spring.HelloWorldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import static org.springframework.web.context.WebApplicationContext.SCOPE_REQUEST;

/**
 * @author Tiberiu Pasat
 */
@Component
@ManagedBean

@Scope(SCOPE_REQUEST)
public class JsfBB {
    @Autowired private HelloWorldService helloWorldService;
    private String name;


    @PostConstruct
    private void init() {
        name = "JSF";

    }

    public String hello() {
        return helloWorldService.hello(name);
    }


}
