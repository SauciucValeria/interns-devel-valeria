package com.example.demo.jsf;

/**
 * Created by Sauciuc on 4/28/2017.
 */
public enum Transmission {
    Manual(1),
    Automatic(2);


    private int label;

    private Transmission(int label) {
        this.label = label;
    }

    public int getLabel() {
        return label;
    }

    public static Transmission getValue(int myLabel) {
        for(Transmission e: Transmission.values()) {
            if(e.label == myLabel) {
                return e;
            }
        }
        return null;// not found
    }


}
