package com.example.demo.jsf;

/**
 * Created by Sauciuc on 4/13/2017.
 */
public enum Fuel {
    Gas(1),
    Diesel(2),
    Petrol(3),
    Electric(4);

    private int label;

    private Fuel(int label) {
        this.label = label;
    }

    public int getLabel() {
        return label;
    }

    public static Fuel getValue(int myLabel) {
        for(Fuel e: Fuel.values()) {
            if(e.label == myLabel) {
                return e;
            }
        }
        return null;// not found
    }



}
