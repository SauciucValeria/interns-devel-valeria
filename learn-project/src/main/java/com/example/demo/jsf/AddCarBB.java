package com.example.demo.jsf;

import com.example.demo.jpa.model.CarEntity;
import com.example.demo.jpa.model.CarModelEntity;
import com.example.demo.jpa.model.CategoryEntity;
import com.example.demo.jpa.model.ProviderEntity;
import com.example.demo.spring.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;
import javax.servlet.http.Part;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sauciuc on 4/26/2017.
 */

@Component
@ManagedBean
@Scope("request")
public class AddCarBB {
    private List<CarEntity> carList;
    private String newModel;
    private Long categoryId;
    private CategoryEntity selectedCategoryEntity;
    private CarModelEntity selectedModel;
    private boolean showEditPopup;
    private boolean showDeletePopup;
    private CarEntity currentCar;
    private String modelName;
    private Long categoryID;
    private List<CategoryEntity> categoriesList;
    private List<CarModelEntity> modelList;
    private List<ProviderEntity> providerList;
    private Part image;


    @Autowired
    private CarServiceImpl carService;
    @Autowired
    private ModelServiceImpl modelService;
    @Autowired
    private CategoryServiceImpl categoryService;
    @Autowired
    private ProviderServiceImpl providerService;


    @Autowired
    private CategoryServiceImpl categService;

    public List<CarModelEntity> getModelList() {
        return modelList;
    }

    public void setModelList(List<CarModelEntity> modelList) {
        this.modelList = modelList;
    }

    public List<CarModelEntity> populateModelList() {
        return (List<CarModelEntity>) modelService.findAllByCategId(this.selectedCategoryEntity.getId());
    }

    public void changeCateg() {
        this.modelList = populateModelList();
    }

    @PostConstruct
    private void init() {

        this.currentCar = new CarEntity();
        carList = populateCarList();
        this.showEditPopup = false;
        this.categoriesList = populateCategList();
        this.providerList = populateProviderList();
        this.modelList = (List<CarModelEntity>) modelService.findAll();

        //this.modelList = populateModelList();
    }

    public void validateFile(FacesContext ctx, UIComponent comp, Object value) {
        List<FacesMessage> msgs = new ArrayList<FacesMessage>();
        Part file = (Part) value;
        if (file.getSize() > 1024) {
            msgs.add(new FacesMessage("file too big"));
        }
        if (!"text/plain".equals(file.getContentType())) {
            msgs.add(new FacesMessage("not a text file"));
        }
        if (!msgs.isEmpty()) {
            throw new ValidatorException(msgs);
        }
    }

    public List<ProviderEntity> populateProviderList() {
        return (List<ProviderEntity>) providerService.findAll();
    }

    public List<CategoryEntity> populateCategList() {
        return (List<CategoryEntity>) categoryService.findAll();
    }

    private List<CarEntity> populateCarList() {
        return (List<CarEntity>) carService.findAll();
    }

    public List<CarEntity> getCarList() {
        return carList;
    }

    public void setCarList(List<CarEntity> carList) {
        this.carList = carList;
    }

    public String getNewModel() {
        return newModel;
    }

    public void setNewModel(String newModel) {
        this.newModel = newModel;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public CategoryEntity getSelectedCategoryEntity() {
        return selectedCategoryEntity;
    }

    public void setSelectedCategoryEntity(CategoryEntity selectedCategoryEntity) {
        this.selectedCategoryEntity = selectedCategoryEntity;
    }

    public CarModelEntity getSelectedModel() {
        return selectedModel;
    }

    public void setSelectedModel(CarModelEntity selectedModel) {
        this.selectedModel = selectedModel;
    }

    public boolean isShowEditPopup() {
        return showEditPopup;
    }

    public void setShowEditPopup(boolean showEditPopup) {
        this.showEditPopup = showEditPopup;
    }

    public boolean isShowDeletePopup() {
        return showDeletePopup;
    }

    public void setShowDeletePopup(boolean showDeletePopup) {
        this.showDeletePopup = showDeletePopup;
    }

    public CarEntity getCurrentCar() {
        return currentCar;
    }

    public void setCurrentCar(CarEntity currentCar) {
        this.currentCar = currentCar;
    }

    public Fuel[] getFuel() {
        return Fuel.values();
    }

    public Transmission[] getTransmission() {
        return Transmission.values();
    }

    public PolluantEmissions[] getPolluantEmissions() {
        return PolluantEmissions.values();
    }

    public String add() {
        currentCar.setCarModelEntity(selectedModel);
        this.carService.saveCar(currentCar);
        carList = populateCarList();
        return "viewCarPage";
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public Long getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(Long categoryID) {
        this.categoryID = categoryID;
    }

    public List<CategoryEntity> getCategoriesList() {
        return categoriesList;
    }

    public void setCategoriesList(List<CategoryEntity> categoriesList) {
        this.categoriesList = categoriesList;
    }

    public List<ProviderEntity> getProviderList() {
        return providerList;
    }

    public void setProviderList(List<ProviderEntity> providerList) {
        this.providerList = providerList;
    }

    public Part getImage() {
        return image;
    }

    public void setImage(Part image) {
        this.image = image;
    }

    public void submit() {
        int a = 0;
    }

    public void handleFileUpload(ValueChangeEvent event) {
        int a = 0;
    }
}
