package com.example.demo.jsf;


import com.example.demo.jpa.model.ProviderDetailsEntity;
import com.example.demo.jpa.model.ProviderEntity;

import com.example.demo.spring.ProviderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.List;

/**
 * Created by Sauciuc on 5/2/2017.
 */

@Component
@ManagedBean
@Scope("request")
public class AddProviderBB {

    private List<ProviderEntity> providers;
    private ProviderEntity newProvider;
    //private boolean showEditPopup;
    //private boolean showDeletePopup;
    private ProviderEntity selectedProvider;
    //private boolean showViewPopup;

    @Autowired
    private ProviderServiceImpl providerService;

    @PostConstruct
    private void init() {
        providers = this.populateProviders();
        ProviderDetailsEntity details = new ProviderDetailsEntity();
        this.newProvider = new ProviderEntity();
        this.newProvider.setProviderDetails(details);
        details.setProvider(newProvider);

    }



    public List<ProviderEntity> populateProviders(){
        return (List<ProviderEntity>) this.providerService.findAll();
    }

    public List<ProviderEntity> getProviders() {
        return providers;
    }

    public void setProviders(List<ProviderEntity> providers) {
        this.providers = providers;
    }

    public ProviderEntity getNewProvider() {
        return newProvider;
    }

    public void setNewProvider(ProviderEntity newProvider) {
        this.newProvider = newProvider;
    }


    public void addNewProvider() {
        ProviderEntity prov = new ProviderEntity();
        ProviderDetailsEntity details = new ProviderDetailsEntity();
        prov.setProviderDetails(details);
        details.setProvider(prov);

        prov.setName(newProvider.getName());
        prov.getProviderDetails().setAddress(newProvider.getProviderDetails().getAddress());
        prov.getProviderDetails().setCity(newProvider.getProviderDetails().getCity());
        prov.getProviderDetails().setCountry(newProvider.getProviderDetails().getCountry());
        providerService.saveProvider(newProvider);
        providers = this.populateProviders();
        this.newProvider =  null;

    }

    public void deleteProvider(ProviderEntity provider) {
        this.selectedProvider = provider;
        providerService.delete(this.selectedProvider);
    }

    public void updateProvider(){
        providerService.updateProvider(this.newProvider);
    }



    public ProviderEntity getSelectedProvider() {
        return selectedProvider;
    }

    public void setSelectedProvider(ProviderEntity selectedProvider) {
        this.selectedProvider = selectedProvider;
    }



    public void saveEdit(ProviderEntity provider) {
        this.providerService.updateProvider(provider);
        this.providers = populateProviders();
        //this.setShowEditPopup(false);
    }

    public void confirmDelete(ProviderEntity provider) {

        this.providerService.delete(provider);
        this.providers = populateProviders();

//        this.setShowDeletePopup(false);
    }
}
