package com.example.demo.jsf;

import com.example.demo.jpa.model.CategoryEntity;
import com.example.demo.spring.CategoryServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import java.util.List;


/**
 * Created by Sauciuc on 4/21/2017.
 */

@Component
@ManagedBean
@Scope("request")

public class AddCategoryBB {

    private List<CategoryEntity> categoryEntityList;
    private CategoryEntity newCategory;
    private CategoryEntity selectedCategoryEntity;


    @Autowired
    private CategoryServiceImpl categoryService;

    @PostConstruct
    private void init() {
        this.newCategory = new CategoryEntity();

        this.categoryEntityList = populateList();
    }




    public List<CategoryEntity> getCategoryEntityList() {
        return categoryEntityList;
    }

    public List<CategoryEntity> populateList(){
        return  (List<CategoryEntity>) categoryService.findAll();
    }

    public void setCategoryEntityList(List<CategoryEntity> categoryEntityList) {
        this.categoryEntityList = categoryEntityList;
    }

    public CategoryEntity getNewCategory() {
        return newCategory;
    }

    public void setNewCategory(CategoryEntity newCategory) {
        this.newCategory = newCategory;
    }

    public void addNewCategory(){
        CategoryEntity categ = new CategoryEntity();
        if (newCategory.getName().compareTo("") == 0){
            FacesContext.getCurrentInstance().addMessage("categoryForm", new FacesMessage(" The name field cannot be left empty! "));
        }else{
            categ.setName(this.newCategory.getName());
            try{
                categoryService.saveCategory(categ);

                this.newCategory = new CategoryEntity();
                this.categoryEntityList = populateList();

            }catch (DataIntegrityViolationException e){
                e.printStackTrace();
                FacesContext.getCurrentInstance().addMessage("categoryForm", new FacesMessage(" The brand is already inserted! "));
            }
            catch (Exception e){
                e.printStackTrace();
                FacesContext.getCurrentInstance().addMessage("categoryForm", new FacesMessage(" Error at inserting in database! "));
            }

        }

    }


    public CategoryEntity getSelectedCategoryEntity() {
        return selectedCategoryEntity;
    }

    public void setSelectedCategoryEntity(CategoryEntity selectedCategoryEntity) {
        this.selectedCategoryEntity = selectedCategoryEntity;
    }


    public void saveEdit(CategoryEntity categ){
        categoryService.updateCategory(categ);

        this.categoryEntityList = populateList();
    }


    public void deleteCategory(CategoryEntity categoryEntity) {
        this.selectedCategoryEntity = categoryEntity;
        categoryService.delete(selectedCategoryEntity);
        this.categoryEntityList = populateList();
    }





}
