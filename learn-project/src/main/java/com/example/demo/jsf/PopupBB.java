package com.example.demo.jsf;

import com.example.demo.jpa.model.CarEntity;
import com.example.demo.spring.CarServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import java.util.List;

/**
 * Created by Sauciuc on 4/18/2017.
 */

@Component
@ManagedBean
@ViewScoped

public class PopupBB {
    private boolean showPopup;
    private CarEntity selectedCarEntity;
    private List<CarEntity> carList;

    @Autowired
    private CarServiceImpl carService;
    private Boolean showViewPopup;
    private Boolean showDeletePopup;

    public CarEntity getSelectedCarEntity() {
        return selectedCarEntity;
    }

    public void setSelectedCarEntity(CarEntity selectedCarEntity) {
        this.selectedCarEntity = selectedCarEntity;
    }

    @PostConstruct
    private void init() {

    }

        public void show(CarEntity carEntity){
        this.selectedCarEntity = carEntity;
        this.showPopup = true;

    }

    public List<CarEntity> populateCarList(){
            return (List<CarEntity>) carService.findAll();
    }

    public void hide(){
        this.setShowPopup(false);
        carService.updateCar(selectedCarEntity);

    }

    public List<CarEntity> getCarList() {
        return carList;
    }

    public void setCarList(List<CarEntity> carList) {
        this.carList = carList;
    }

    public boolean isShowPopup() {
        return showPopup;
    }

    public void setShowPopup(boolean showPopup) {
        this.showPopup = showPopup;
    }

    public void setShowViewPopup(Boolean showViewPopup) {
        this.showViewPopup = showViewPopup;
    }

    public Boolean getShowViewPopup() {
        return showViewPopup;
    }

    public void showView(CarEntity carEntity) {
        this.selectedCarEntity = carEntity;
        this.setShowViewPopup(true);
    }

    public void hideViewPopup() {
        this.setShowViewPopup(false);
    }

    public void cancelEdit() {
        this.setShowPopup(false);
        this.setShowViewPopup(false);
    }

    public void setShowDeletePopup(Boolean showDeletePopup) {
        this.showDeletePopup = showDeletePopup;
    }

    public Boolean getShowDeletePopup() {
        return showDeletePopup;
    }

    public void showDelete(CarEntity carEntity) {
        this.selectedCarEntity = carEntity;
        this.setShowDeletePopup(true);
    }

    public void confirmDelete() {
        this.setShowDeletePopup(false);
        this.carService.delete(selectedCarEntity);
        this.carList = populateCarList();
    }

    public void cancelDelete() {
        this.setShowDeletePopup(false);
    }
}
