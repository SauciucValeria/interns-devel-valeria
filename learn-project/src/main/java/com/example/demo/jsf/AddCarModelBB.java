package com.example.demo.jsf;

import com.example.demo.jpa.model.CarModelEntity;
import com.example.demo.jpa.model.CategoryEntity;
import com.example.demo.spring.CategoryServiceImpl;
import com.example.demo.spring.ModelServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Sauciuc on 4/25/2017.
 */

@Component
@ManagedBean
@Scope("request")
public class AddCarModelBB {


    private List<CarModelEntity> modelList;
    private String newModel;
    private Long categoryId;
    private CategoryEntity categorySelected;
    private List<CategoryEntity> categoryList;

    @Autowired
    private ModelServiceImpl modelService;
    @Autowired
    private CategoryServiceImpl categoryService;

    @PostConstruct
    private void init() {
        modelList = populateList();
        categoryList = populateCategList();
    }



    public List<CategoryEntity> populateCategList(){
        return (List<CategoryEntity>) categoryService.findAll();
    }

    public List<CarModelEntity> getModelList() {
        return modelList;
    }

    public List<CarModelEntity> populateList(){
        return (List<CarModelEntity>) modelService.findAll();
    }

    public void setModelList(List<CarModelEntity> modelList) {
        this.modelList = modelList;
    }

    public String getNewModel() {
        return newModel;
    }

    public void setNewModel(String newModel) {
        this.newModel = newModel;
    }



    public void addNewModel() {
        if (this.newModel.compareTo("") == 0){
            FacesContext.getCurrentInstance().addMessage("modelForm", new FacesMessage("The name field cannot be left empty!"));
        }else {
            CarModelEntity model = new CarModelEntity();
            this.categorySelected = categoryService.findOne(this.categoryId);
            model.setName(this.newModel);
            model.setCategoryEntity(this.categorySelected);

            try{
                modelService.saveModel(model);
                modelList = (List<CarModelEntity>) modelService.findAll();
            }catch (DataIntegrityViolationException e){
                e.printStackTrace();
                FacesContext.getCurrentInstance().addMessage("modelForm", new FacesMessage(" The model is already inserted! "));
            }catch (Exception e){
                e.printStackTrace();
                FacesContext.getCurrentInstance().addMessage("modelForm", new FacesMessage(" Error at inserting in database! "));
            }


        }
    }


    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public void saveEditPopup(CarModelEntity model) {

        modelService.updateModel(model);
        this.modelList = populateList();

    }


    public void deleteCarModel(CarModelEntity model) {
        this.modelService.delete(model);
        this.modelList = populateList();
    }




    public List<CategoryEntity> getCategoryList() {
        return categoryList;
    }



    public void setCategoryList(List<CategoryEntity> categoryList) {
        this.categoryList = categoryList;
    }


}
