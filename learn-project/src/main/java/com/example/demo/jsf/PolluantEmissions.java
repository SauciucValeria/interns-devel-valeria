package com.example.demo.jsf;

/**
 * Created by Sauciuc on 4/28/2017.
 */
public enum PolluantEmissions {
    Euro1(1),
    Euro2(2),
    Euro3(3),
    Euro4(4),
    Euro5(5),
    Euro6(6);


    private int label;

    private PolluantEmissions(int label) {
        this.label = label;
    }

    public int getLabel() {
        return label;
    }

    public static PolluantEmissions getValue(int myLabel) {
        for(PolluantEmissions e: PolluantEmissions.values()) {
            if(e.label == myLabel) {
                return e;
            }
        }
        return null;// not found
    }


}
