package com.example.demo.jsf;

import com.example.demo.jpa.model.CarEntity;
import com.example.demo.jpa.model.CarModelEntity;
import com.example.demo.jpa.model.CategoryEntity;
import com.example.demo.spring.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;

import java.util.*;

@Component
@ManagedBean
@Scope("request")

public class CarListBB {

    @Autowired private CarServiceImpl carService;

    @Autowired
    private ModelServiceImpl modelService;
    @Autowired
    private CategoryServiceImpl categoryService;


    private String selectedItem;
    private String selectedFuel;
    private List<CarEntity> carEntities;
    private CarEntity currentCarEntity;
    private List<CategoryEntity> categoriesList;
    private List<CarModelEntity> modelList;
    private CategoryEntity selectedCateg;

    @PostConstruct
    private void init() {
        carEntities = populateList();
        currentCarEntity = new CarEntity();
        this.categoriesList = populateCategList();
        this.modelList = populateModelList();

    }

    public List<CarModelEntity> populateModelList(){
        return (List<CarModelEntity>) modelService.findAll();
    }

    public void populateByCateg(CategoryEntity categ){
        this.modelList = (List<CarModelEntity>) modelService.findAllByCategId(categ.getId());
    }

    public void changeCateg(CategoryEntity categ){
        populateByCateg(categ);
    }

    public CategoryEntity getSelectedCateg() {
        return selectedCateg;
    }

    public void setSelectedCateg(CategoryEntity selectedCateg) {
        this.selectedCateg = selectedCateg;
    }

    public List<CategoryEntity> populateCategList(){
        return (List<CategoryEntity>) categoryService.findAll();
    }

    public List<CarEntity> populateList(){
        return (List<CarEntity>) carService.findAll();
    }

    public String getSelectedFuel() {
        return selectedFuel;
    }

    public void setSelectedFuel(String selectedFuel) {
        this.selectedFuel = selectedFuel;
    }

    public String getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(String selectedItem) {
        this.selectedItem = selectedItem;
    }


    public List<CarModelEntity> getModelList() {
        return modelList;
    }

    public void setModelList(List<CarModelEntity> modelList) {
        this.modelList = modelList;
    }

    public List<CarEntity> getCarEntities() {
        return carEntities;
    }

    public void setCarEntities(List<CarEntity> carEntities) {
        this.carEntities = carEntities;
    }

    public CarEntity getCurrentCarEntity() {
        return currentCarEntity;
    }

    public void setCurrentCarEntity(CarEntity currentCarEntity) {
        this.currentCarEntity = currentCarEntity;
    }


    public void confirmDelete(CarEntity car) {
        this.carService.delete(car);
        this.carEntities = populateList();
    }

    public void saveEditPopup(CarEntity car) {
        this.carService.updateCar(car);
        this.carEntities = this.populateList();

    }

    public List<CategoryEntity> getCategoriesList() {
        return categoriesList;
    }

    public void setCategoriesList(List<CategoryEntity> categoriesList) {
        this.categoriesList = categoriesList;
    }
}
