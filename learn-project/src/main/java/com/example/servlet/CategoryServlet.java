package com.example.servlet;

import com.example.demo.jpa.model.CarEntity;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Sauciuc on 4/11/2017.
 */

@Component

public class CategoryServlet extends HttpServlet{


    //@Inject
    private CarEntity carEntity;


    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String name = "carType";
        String value = request.getParameter(name);

        HttpSession session = request.getSession(true);
        CarEntity carEntity = (CarEntity)session.getAttribute("carEntity");
//        carEntity.setType(value);
        if (carEntity !=null) {
            System.out.println(carEntity);
        }
        response.setContentType("text/html");

        // New location to be redirected
        String site = new String("/addCategoryHTML.html");

        response.setStatus(response.SC_MOVED_TEMPORARILY);
        response.setHeader("Location", site);


    }



}
